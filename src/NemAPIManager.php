<?php

namespace gladwelln\nem;

use Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class NemAPIManager
{
    private $debug = 1;
    
    /**
     * Make a GET HTTP request
     *
     * * @param string $path
     * * @param string[] $params
     *
     * @return array
     */
    public function getJson($path = '/', $params = [])
    {
        if ($this->debug)
        {
            Log::info('=======================================================getJson====================================================================');
        }

        $response = $this->request('GET', $path, $params);

        return $response;
    }

    /**
     * Make a POST HTTP request
     *
     * * @param string $path
     * * @param string[] $params
     * * @param string[] $data
     *
     * @return array
     */
    public function postJson($path = '/', $params = [], $data = [])
    {
        if ($this->debug)
        {
            Log::info('=======================================================postJson====================================================================');
        }

        $response = $this->request('POST', $path, $params, $data);

        return $response;
    }

    /**
     * Make an HTTP request
     *
     * * @param string $type
     * * @param string $path
     * * @param string[] $params
     * * @param string[] $data
     *
     * @return array
     */
    private function request($type = 'GET', $path = '/', $params = [], $data = [])
    {
        $options = $this->getOptions($params, $data);
        
        $response = $this->sendRequest($type, $path, $options);
        
        return $response;
    }

    /**
     * Create request headers, query string and body content.
     *
     * * @param string[] $params
     * * @param string[] $data
     *
     * @return array
     */
    private function getOptions($params, $data)
    {
        $options = [
            'query' => $params,
            'body' => json_encode($data),
            'headers' => [
                'Content-Type' => 'application/json',
                //'Pragma' => 'no-cache',
                //'Cache-Control' => 'no-cache'
            ],
            //'debug' => true
        ];

        return $options;
    }

    /**
     * Send API request
     *
     * * @param string $type
     * * @param string $path
     * * @param string[] $options
     *
     * @return array
     */
    private function sendRequest($type, $path, $options)
    {
        $node = env('NODE_BASE_URL', 'http://127.0.0.1:7890');
        $url = $node . "". $path;
        
        if ($this->debug)
        {
            Log::info("$type request to url: " . $url . " :: ", $options);
        }
        
        $client = new Client();
        
        $contents['status'] = false;

        try
        {
            $response = $client->request($type, $url, $options);
            $body = $response->getBody();
            $contents['status'] = true;
            $contents['payload'] = $body->getContents();
        } 
        catch (RequestException $ex)
        {
            $contents['error_description'] = 'Invalid request: ' . $ex->getMessage();
        }
        catch (ConnectException $ex)
        {
            $contents['error_description'] = 'A networking error occurred: ' . $ex->getMessage();
        } 
        catch (ClientException $ex)
        {
            $contents['error_description'] = 'A bad request occurred: ' . $ex->getMessage();
        } 
        catch (ServerException $ex)
        {
            $contents['error_description'] = 'Server error occurred: ' . $ex->getMessage();
        }

        return $contents;
    }
}

?>
