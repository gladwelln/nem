<?php

namespace gladwelln\nem\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use NemAPI;

class AccountController extends Controller
{
    public function __construct() { }

    public function generate_new_account()
    {
        $response = NemAPI::getJson('/account/generate');
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_form($using)
    {
        $html = view('nem::elements.account.form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_data(Request $request)
    {
        $value = str_replace('-', '', $request->get('value'));
        $using = $request->get('using');
        $path = '/account/get';

        if($using == 'address')
        {
            $key = 'address';
        }
        elseif($using == 'public_key')
        {
            $key = 'publicKey';
            $path .= '/from-public-key';
        }
        elseif($using == 'deligate_acc')
        {
            $key = 'address';
            $path .= '/forwarded';
        }
        elseif($using == 'acc_status')
        {
            $key = 'address';
            $path = '/account/status';
        }

        $response = NemAPI::getJson($path, [$key => $value]);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_transaction_form($using)
    {
        $html = view('nem::elements.account.transaction-form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_transaction_data(Request $request)
    {
        $address = str_replace('-', '', $request->get('address'));
        $hash = $request->get('hash');
        $using = $request->get('using');
        $path = '/account';

        if($using == 'incoming')
        {
            $path .= '/transfers/incoming';
        }
        elseif($using == 'outgoing')
        {
            $path .= '/transfers/outgoing';
        }
        elseif($using == 'all')
        {
            $path .= '/transfers/all';
        }
        elseif($using == 'unconfirmed')
        {
            $path .= '/unconfirmedTransactions';
        }
        elseif($using == 'harvests')
        {
            $path .= '/harvests';
        }

        $params = ['address' => $address];
        
        if(!empty($hash))
        {
            $params['hash'] = $hash;
        }
        
        $response = NemAPI::getJson($path, $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_importances()
    {
        $response = NemAPI::getJson('/account/importances');
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function lock_unclock_account($action)
    {
        $response = NemAPI::postJson("/account/$action", [], ['value' => env('TEST_PRVT_KEY', '******')]);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_unclock_info()
    {
        $response = NemAPI::postJson("/account/unlocked/info");
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_namespace_form()
    {
        $html = view('nem::elements.account.namespace-form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_namespace_data(Request $request)
    {
        $address = str_replace('-', '', $request->get('address'));
        $parent = $request->get('parent');

        $params = ['address' => $address];
        if(!empty($parent))
        {
            $params['parent'] = $parent;
        }

        $response = NemAPI::getJson("/account/namespace/page", $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_mosaic_definitions_form()
    {
        $html = view('nem::elements.account.mosaic-definitions-form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_mosaic_definitions_data(Request $request)
    {
        $address = str_replace('-', '', $request->get('address'));
        $parent = $request->get('parent');

        $params = ['address' => $address];
        if(!empty($parent))
        {
            $params['parent'] = $parent;
        }

        $response = NemAPI::getJson("/account/mosaic/definition/page", $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_mosaics_form()
    {
        $html = view('nem::elements.account.mosaics-form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_mosaics_data(Request $request)
    {
        $address = str_replace('-', '', $request->get('address'));

        $params = ['address' => $address];

        $response = NemAPI::getJson("/account/mosaic/owned", $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }
}