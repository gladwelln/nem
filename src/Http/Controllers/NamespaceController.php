<?php

namespace gladwelln\nem\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use NemAPI;

class NamespaceController extends Controller
{
    public function __construct() { }

    public function get_root_namespace_form()
    {
        $html = view('nem::elements.namespace.root-form', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_root_namespace_data(Request $request)
    {
        $id = $request->get('id');
        $pagesize = $request->get('pagesize');
        $params = [];

        if(!empty($id))
        {
            $params['id'] = $id;
        }
        if(!empty($pagesize))
        {
            $params['pageSize'] = $pagesize;
        }

        $response = NemAPI::getJson('/namespace/root/page', $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_namespace_form()
    {
        $html = view('nem::elements.namespace.form', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_namespace_data(Request $request)
    {
        $namespace = $request->get('namespace');
        $params = [];

        if(!empty($namespace))
        {
            $params['namespace'] = $namespace;
        }

        $response = NemAPI::getJson('/namespace', $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_mosaic_definitions_form()
    {
        $html = view('nem::elements.namespace.mosaic-definitions-form', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_mosaic_definitions_data(Request $request)
    {
        $namespace = $request->get('namespace');
        $id = $request->get('id');
        $pagesize = $request->get('pagesize');
        $params = ['namespace' => $namespace];

        if(!empty($id))
        {
            $params['id'] = $id;
        }
        if(!empty($pagesize))
        {
            $params['pageSize'] = $pagesize;
        }

        $response = NemAPI::getJson('/namespace/mosaic/definition/page', $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }
}