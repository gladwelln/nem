<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['namespace' => '\gladwelln\nem\Http\Controllers', 'middleware' => 'web'], function()
{
    Route::get('/', 'DashboardController@index');

    //NIS status related requests
    Route::get('ajax_check_nis_heartbeat', 'NisController@check_nis_heartbeat');
    Route::get('ajax_check_nis_status', 'NisController@check_nis_status');
    Route::get('ajax_get_time_synchronization', 'NisController@get_time_synchronization');
    Route::get('ajax_get_calls/{calls}', 'NisController@get_calls');
    Route::get('ajax_get_timers', 'NisController@get_timers');

    //Account related requests
    Route::get('ajax_generate_new_account', 'AccountController@generate_new_account');
    Route::get('ajax_get_account_form/{using}', 'AccountController@get_account_form');
    Route::post('ajax_get_account_data', 'AccountController@get_account_data');
    Route::get('ajax_get_account_transaction_form/{using}', 'AccountController@get_account_transaction_form');
    Route::post('ajax_get_account_transaction_data', 'AccountController@get_account_transaction_data');
    Route::get('ajax_get_account_namespace_form', 'AccountController@get_account_namespace_form');
    Route::post('ajax_get_account_namespace_data', 'AccountController@get_account_namespace_data');
    Route::get('ajax_get_account_mosaic_definitions_form', 'AccountController@get_account_mosaic_definitions_form');
    Route::post('ajax_get_account_mosaic_definitions_data', 'AccountController@get_account_mosaic_definitions_data');
    Route::get('ajax_get_account_mosaics_form', 'AccountController@get_account_mosaics_form');
    Route::post('ajax_get_account_mosaics_data', 'AccountController@get_account_mosaics_data');
    Route::get('ajax_get_account_importances', 'AccountController@get_account_importances');
    Route::get('ajax_lock_unclock_account/{action}', 'AccountController@lock_unclock_account');
    Route::get('ajax_get_account_unclock_info', 'AccountController@get_account_unclock_info');
    
    //Block chain related requests
    Route::get('ajax_get_block_chain_info/{action}', 'NisController@get_block_chain_info');
    Route::get('ajax_get_block_chain_height_form/{block}', 'NisController@get_block_chain_height_form');
    Route::post('ajax_get_block_chain_height_data', 'NisController@get_block_chain_height_data');

    //Node related requests
    Route::get('ajax_get_node_info/{action}', 'NisController@get_node_info');
    Route::get('ajax_get_peer_nodes/{which_nodes}', 'NisController@get_peer_nodes');
    Route::get('ajax_get_active_peer_nodes_height', 'NisController@get_active_peer_nodes_height');
    Route::get('ajax_get_node_experiences', 'NisController@get_node_experiences');
    Route::get('ajax_boot_local_node', 'NisController@boot_local_node');

    //Namespaces and Mosaics
    Route::get('ajax_get_root_namespace_form', 'NamespaceController@get_root_namespace_form');
    Route::post('ajax_get_root_namespace_data', 'NamespaceController@get_root_namespace_data');
    Route::get('ajax_get_namespace_form', 'NamespaceController@get_namespace_form');
    Route::post('ajax_get_namespace_data', 'NamespaceController@get_namespace_data');
    Route::get('ajax_get_mosaic_definitions_form', 'NamespaceController@get_mosaic_definitions_form');
    Route::post('ajax_get_mosaic_definitions_data', 'NamespaceController@get_mosaic_definitions_data');

    //Transactions related requests
    Route::get('ajax_get_transaction_form/{using}', 'TransactionController@get_transaction_form');
    Route::post('ajax_submit_transaction', 'TransactionController@submit_transaction');
    Route::get('ajax_get_account_historical_data_form', 'TransactionController@get_account_historical_data_form');
    Route::post('ajax_get_account_historical_data', 'TransactionController@get_account_historical_data');
    Route::post('ajax_submit_multisig_transaction', 'TransactionController@submit_multisig_transaction');
});