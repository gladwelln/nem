function checkNisHeartbeat()
{
    $.doAJAX(base_url + '/ajax_check_nis_heartbeat', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function checkNisStatus()
{
    $.doAJAX(base_url + '/ajax_check_nis_status', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getBlockChainInfo(action)
{
    $.doAJAX(base_url + '/ajax_get_block_chain_info/' + action, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getBlockChainHeightForm(block)
{
    $.doAJAX(base_url + '/ajax_get_block_chain_height_form/' + block, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getBlockChainHeightData(block)
{
    var height = $('#height').val();
    if(!height)
    {
        notify("warning", 'Please provide a height to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_block_chain_height_data', {'block': block, 'height': height}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getNodeInfo(action)
{
    $.doAJAX(base_url + '/ajax_get_node_info/' + action, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getPeerNodes(which_nodes)
{
    $.doAJAX(base_url + '/ajax_get_peer_nodes/' + which_nodes, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getActivePeerNodesHeight()
{
    $.doAJAX(base_url + '/ajax_get_active_peer_nodes_height', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getNodeExperiences()
{
    $.doAJAX(base_url + '/ajax_get_node_experiences', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function bootLocalNode()
{
    $.doAJAX(base_url + '/ajax_boot_local_node', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getTimeSynchronization()
{
    $.doAJAX(base_url + '/ajax_get_time_synchronization', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getCalls(calls)
{
    $.doAJAX(base_url + '/ajax_get_calls/' + calls, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getTimers()
{
    $.doAJAX(base_url + '/ajax_get_timers', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}