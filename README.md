# Laravel NEM NIS API end point test
A laravel package to test NEM NIS API end points.

## Installation

[PHP](https://php.net) 7+, [Laravel](https://laravel.com/docs/5.4) 5.4+ and [Composer](https://getcomposer.org) are required.

To get the latest version of Laravel nem, simply add the following to your `composer.json` file.

```
"repositories": [
  	{
        "type": "git",
    	"url": "https://gladwelln@bitbucket.org/gladwelln/nem.git"
	}
],

"require": {
	"gladwelln/nem": "dev-master"
}
```

You'll then need to run `composer install` or `composer update` to download it and have the autoloader updated.

If you are using Laravel 5.5 or later, the service provider auto discovery feature will load the required classes after installation otherwise, you need to register the service provider.
Open up `config/app.php` and add the following to the `providers` key.

* `gladwelln\nem\NemServiceProvider::class`


## Configuration

You need to add below to your .env file:

```bash
NODE_BASE_URL=http://127.0.0.1:7890
TEST_PRVT_KEY=***********
TEST_PBLC_KEY=***********
MULTISIG_PBLC_KEY=***********
```

Then you are ready to get running. Just visit and start testing the end points as per [NEM NIS API Documentation](https://nemproject.github.io/)

```bash
http://{{site-url}}/
```