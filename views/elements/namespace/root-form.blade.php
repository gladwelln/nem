<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control input-lg" id="id" placeholder="Enter your id here...">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control input-lg" id="pagesize" placeholder="Enter page size here...">
                </div>
            </div>
            <span class="input-group-btn">
                <button class="btn btn-success btn-lg" type="button" onclick="getRootNamespaceData()">Go!</button>
            </span>
        </div>
    </div>
    <div class="panel-body" id="namespace-content-holder">
        <h3 class="text-info text-center">Enter your id and or pagesize above to continue.</h3>
    </div>
</div>