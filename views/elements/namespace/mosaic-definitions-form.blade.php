<div class="panel panel-default">
    <div class="panel-heading">
        Retrieving mosaic definitions
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">  
                <form class="form-horizontal" onSubmit="return false;">
                    <div class="form-group">
                        <label for="namespace" class="col-sm-4 control-label">Namespace:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="namespace" placeholder="Namespace">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="id" class="col-sm-4 control-label">Id:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="id" placeholder="id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pagesize" class="col-sm-4 control-label">Page size:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pagesize" placeholder="pagesize">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="button" class="btn btn-success btn-block" onClick="getMosaicDefinitonsData()">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <div class="col-lg-12" id="namespace-content-holder">
                <!-- AJAX Content here -->
            </div>
        </div>
    </div>
</div>