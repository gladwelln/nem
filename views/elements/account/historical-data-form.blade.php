<div class="panel panel-default">
    <div class="panel-heading">
        Historical account data
    </div>
    <div class="panel-body" id="account-content-holder">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" onSubmit="return false;">
                    <div class="form-group">
                        <label for="address" class="col-sm-4 control-label">Address:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="address" placeholder="Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="start-height" class="col-sm-4 control-label">Start Height:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="start-height" placeholder="Start height">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end-height" class="col-sm-4 control-label">End Height:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="end-height" placeholder="End height">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="increment" class="col-sm-4 control-label">Increment:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="increment" placeholder="Increment">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="button" class="btn btn-success btn-block" onClick="getAccountHistoricalData()">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <div class="col-lg-12" id="historical-content-holder">
            <!-- AJAX Content here -->
            </div>
        </div>
    </div>
</div>