<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group input-group-lg">
            <input type="text" class="form-control" id="{!! $using !!}">
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" onclick="getAccountData('{!! $using !!}')">Go!</button>
            </span>
        </div>
    </div>
    <div class="panel-body" id="account-content-holder">
        <h3 class="text-info text-center">Enter your {!! ($using == 'public_key') ? 'public key' : 'address' !!} above to continue.</h3>
    </div>
</div>