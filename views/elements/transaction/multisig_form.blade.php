<div class="panel panel-default">
    <div class="panel-heading">
        Multisig transaction
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">  
                <form class="form-horizontal" onSubmit="return false;">
                    <div class="form-group">
                        <label for="recipient" class="col-sm-4 control-label">To:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="recipient" placeholder="Recipient address">
                        </div>
                    </div>
                    
                    <div class="form-group" id="amount-holder">
                        <label for="amount" class="col-sm-4 control-label">Amount:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control" id="amount" onkeyup="calculateFee(1)" placeholder="Amount . . . . ">
                                <div class="input-group-addon">XEM</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message" class="col-sm-4 control-label">Message:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="message" onkeyup="calculateFee(1)" rows=6 placeholder="Message . . . ."></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="mosaic-checkbox"> Mosaic transfer
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <div style="display: none;" id="mosaic-content-holder">
                        <div class="form-group">
                            <label for="mosaic" class="col-sm-4 control-label">Mosaic:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mosaic" placeholder="Input your mosaic . . E.G dim:coin">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="quantity" class="col-sm-4 control-label">Amount:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="quantity" onkeyup="calculateFee(1)" placeholder="Amount . . . . ">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fee" class="col-sm-4 control-label">Fee:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly id="fee_read_only" value="0.150000 + 0.050000">
                                <input type="hidden" class="form-control" id="fee" value="0.10">
                                <div class="input-group-addon">XEM</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="button" class="btn btn-success btn-block" onClick="sendMultisigTransaction()">Send</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <div class="col-lg-12" id="transaction-content-holder">
                <!-- AJAX Content here -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".mosaic-checkbox").change(function()
    {
        if(this.checked) 
        {
            $('#mosaic-content-holder').fadeIn('slow');
            $('#amount-holder').fadeOut('slow');

            $('#amount-holder').find('input').attr('id', 'useless');
        }
        else
        {
            $('#mosaic-content-holder').fadeOut('slow');
            $('#amount-holder').fadeIn('slow');

            $('#amount-holder').find('input').attr('id', 'amount');
        }

        calculateFee(1);
    });
</script>