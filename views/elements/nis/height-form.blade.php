<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group input-group-lg">
            <input type="text" class="form-control" id="height">
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" onclick="getBlockChainHeightData('{!! $block !!}')">Go!</button>
            </span>
        </div>
    </div>
    <div class="panel-body" id="blockchain-content-holder">
        <h3 class="text-info text-center">Enter block height above to continue.</h3>
    </div>
</div>