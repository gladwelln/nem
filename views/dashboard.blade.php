@extends('nem::layouts.app')

@section('content')

<div class="container-fluid" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-3">
            <ul id="tree" class="">
                <li>
                    <a href="javascript:;">NIS status related requests</a>
                    <ul>
                        <li>
                            <a href="javascript:;" onclick="checkNisHeartbeat()">Heartbeat</a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="checkNisStatus()">Status</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">Account related requests</a>
                    <ul>
                        <li>
                            <a href="javascript:;">Retrieving account data</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="generateNewAccount()">Generate new account data.</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Request account data.</a>
                                    <ul>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountForm('address')">Using Address.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountForm('public_key')">Using Public Key.</a>
                                        </li>
                                        <li>
                                        <a href="javascript:;" onclick="getAccountForm('deligate_acc')">Using a delegate account.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountForm('acc_status')">Account status.</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;">Request account transaction data.</a>
                                    <ul>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountTransactionForm('incoming')">Incoming transactions.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountTransactionForm('outgoing')">Outgoing transactions.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountTransactionForm('all')">All transactions.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountTransactionForm('unconfirmed')">Unconfirmed transactions.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountTransactionForm('harvests')">Harvests.</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="getAccountImportances('importances')">Importances.</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getAccountNamespaceForm()">Account namespaces.</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getAccountMosaicDefinitionsForm()">Account mosaic definitions.</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getAccountMosaicsForm()">Account mosaics.</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Locking and unlocking accounts.</a>
                                    <ul>
                                        <li>
                                            <a href="javascript:;" onclick="lockUnlockAccount('lock')">Lock Account</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="lockUnlockAccount('unlock')">Unlock Account</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getAccountUnlockInfo()">Retrieving the unlock info</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="getAccountHistoricalDataForm()">Historical account data</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">Block chain related requests</a>
                    <ul>
                        <li>
                            <a href="javascript:;">Block chain status information</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getBlockChainInfo('height')">Block chain height</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getBlockChainInfo('score')">Block chain score</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getBlockChainInfo('last-block')">Last block of the block chain score</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">Requesting parts of the block chain</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getBlockChainHeightForm('full')">Getting a block with a given height</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getBlockChainHeightForm('part')">Getting part of a chain</a>
                                </li>
                            </ul>
                        </li>
                    </ul>                        
                </li>
                <li>
                    <a href="javascript:;">Node related requests</a>
                    <ul>
                        <li>
                            <a href="javascript:;">Requesting information about a node</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getNodeInfo('info')">Basic node information</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getNodeInfo('extended-info')">Extended node information</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">Discovering the neighborhood of a node</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getPeerNodes('all')">Complete neighborhood</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getPeerNodes('reachable')">Reachable neighborhood</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getPeerNodes('active')">Active neighborhood</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getActivePeerNodesHeight()">Maximum chain height in the active neighborhood</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="getNodeExperiences()">Requesting node experiences</a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="bootLocalNode()">Booting the local node</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">Namespaces and Mosaics</a>
                    <ul>
                        <li>
                            <a href="javascript:;">Namespaces</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getRootNamespaceForm()">Retrieving root namespaces</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getNamespaceForm()">Retrieving a specific namespace</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">Mosaics</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getMosaicDefinitonsForm()">Retrieving mosaic definitions</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>                                         
                <li>
                    <a href="javascript:;">Initiating transactions</a>
                    <ul>
                        <li>
                            <a href="javascript:;" onclick="getTransactionForm('version1')">Version 1 transfer</a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="getTransactionForm('version2')">Version 2 transfer</a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="getTransactionForm('multisig')">Multisig transaction</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">Requests for additional information from NIS</a>
                    <ul>
                        <li>
                            <a href="javascript:;" onclick="getTimeSynchronization()">Monitoring the network time</a>
                        </li>
                        <li>
                            <a href="javascript:;">Monitoring incoming and outgoing calls</a>
                            <ul>
                                <li>
                                    <a href="javascript:;" onclick="getCalls('incoming')">Incoming</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getCalls('outgoing')">Outgoing</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="getTimers()">Monitoring timers</a>
                        </li>
                    </ul>
                </li>                    
            </ul>
        </div>
        <div class="col-md-9" id="content-holder">
            <!-- AJAX Content here -->
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        //Initialization of treeviews
        $('#tree').treed();
    </script>

    {!! Html::script(url('vendor/nem/js/nis.js')) !!}
    {!! Html::script(url('vendor/nem/js/account.js')) !!}
    {!! Html::script(url('vendor/nem/js/transaction.js')) !!}
    {!! Html::script(url('vendor/nem/js/namespace.js')) !!}
@endpush